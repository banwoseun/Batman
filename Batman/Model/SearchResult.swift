//
//  SearchResult.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 11/02/2022.
//

import SwiftUI

// Search result from API to get iMDBID
struct SearchResult: Codable, Hashable {
    var Search: [ImDBID]
}


