//
//  iMDdb.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 11/02/2022.
//

import SwiftUI

struct ImDBID: Codable, Hashable {
    let imdbID: String
}
