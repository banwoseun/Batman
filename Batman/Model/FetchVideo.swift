//
//  FetchVideo.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 14/02/2022.
//

import SwiftUI

class FetchVideo: ObservableObject {
    @Published var videos: [Video] = []
    var imDBIDs: [ImDBID] = []
    
    let apiKey = "18ba084d"
    var pageNumber = 1
    
    init() {
        fetch()
    }
    
    // Fetch More Movies by page number
    func fetchMore() {
        pageNumber = pageNumber + 1
        fetch()
    }
    
    func fetch() {
        // construct URL from string
        let urlString = "https://www.omdbapi.com/?apikey=\(apiKey)&s=Batman&page=\(pageNumber)"
        
       
        
        DispatchQueue.main.async {
            // [weak self] in
            // Safely convert URL
            if let url = URL(string: urlString) {
                if let data = try? Data(contentsOf: url) {
                    // Parse data
                    parse(json: data)
                }
            }
        }
        
        // Parse Json Data
        func parse(json: Data) {
            let decoder = JSONDecoder()
            if let jsonSearchResult = try? decoder.decode(SearchResult.self, from: json) {
                imDBIDs = jsonSearchResult.Search
                parseImdbId()
            }
        }
        
        func parseImdbId() {
            for imdbid in imDBIDs {
                let urlImdbIdString = "https://omdbapi.com/?apikey=\(apiKey)&i=\(imdbid.imdbID)"
                
                // Safely convert URL
                if let urlImdb = URL(string: urlImdbIdString) {
                    if let data = try? Data(contentsOf: urlImdb) {
                        parseAndAppend(json: data)
                    }
                }
            }
        }
        
        // Parse Json Data and Append
        func parseAndAppend(json: Data) {
            let decoder = JSONDecoder()
            if let jsonSearchResult = try? decoder.decode(Video.self, from: json) {
                videos.append(jsonSearchResult)
            }
        }
    }
    
   
}
