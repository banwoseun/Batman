//
//  VideoFavorites.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 14/02/2022.
//

import SwiftUI

class VideoFavorites: ObservableObject {
    
    @Published var savedFavorites = [Favorites]() {
        didSet {
            if let encoded = try? JSONEncoder().encode(savedFavorites) {
                UserDefaults.standard.set(encoded, forKey: "SavedFavorites")
            }
        }
    }
    
    init() {
        fetchVideoFromUserDefaults()
    }
    
    func fetchVideoFromUserDefaults() {
        if let savedItems = UserDefaults.standard.data(forKey: "SavedFavorites") {
            if let decodedItems = try? JSONDecoder().decode([Favorites].self, from: savedItems) {
                savedFavorites = decodedItems
                return
            }
        }

        savedFavorites = []
    }
    func addToFavorites(video: Video) {
        
        //Add to Favorites
        let favoriteMovie = Favorites(
        Title: video.Title,
            Year: video.Year,
            Rated: video.Rated,
        Released: video.Released,
        Runtime: video.Runtime,
        Genre: video.Genre,
        Director: video.Director,
            Actors: video.Actors,
            Plot: video.Plot,
            Language: video.Language,
            Country: video.Country,
            Awards: video.Awards,
        Poster: video.Poster,
            Metascore: video.Metascore,
            imdbRating: video.imdbRating,
            imdbVotes: video.imdbVotes,
            imdbID: video.imdbID,
            DVD: video.imdbID,
            BoxOffice: video.BoxOffice,
            Production: video.Production,
            Website: video.Website,
            Response: video.Response
        )
       
        DispatchQueue.main.async { [weak self] in
            self?.objectWillChange.send()
            self?.savedFavorites.append(favoriteMovie)
           }
    }
    
    func removeFavorites(at offsets: IndexSet) {
        savedFavorites.remove(atOffsets: offsets)
    }
}


//struct VideoFavorites_Previews: PreviewProvider {
//    static var previews: some View {
//        VideoFavorites()
//    }
//}
