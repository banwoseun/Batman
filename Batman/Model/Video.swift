//
//  Video.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 10/02/2022.
//

import SwiftUI

//struct Video: Codable, Hashable {
//    let Title: String
//    let Year: String
//    let imdbID: String
//    let Poster: String
//}

struct Video: Codable, Hashable {
    let Title: String
    let Year: String
    let Rated: String
    let Released: String
    let Runtime: String
    let Genre: String
    let Director: String
    let Actors: String
    let Plot: String
    let Language: String
    let Country: String
    let Awards: String
    let Poster: String
    let Metascore: String
    let imdbRating: String
    let imdbVotes: String
    let imdbID: String
    let DVD: String
    let BoxOffice: String
    let Production: String
    let Website: String
    let Response: String
}

//  "Ratings":[{"Source":"Internet Movie Database","Value":"6.4/10"},{"Source":"Rotten Tomatoes","Value":"39%"}],
//  "Response":"True" - Boolean

//  var id = UUID()

//  struct Video: Identifiable {
//    let id = UUID()
//    let imageName: String
//    let title: String
//    let description: String
//    let viewCount: Int
//    let uploadDate: String
//    let url: URL
//  }
