//
//  AppDelegate.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 10/02/2022.
//

import SwiftUI

@main
struct Batman: App { 
    
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
