//
//  FavoriteDetailView.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 14/02/2022.
//

import SwiftUI

struct FavoriteDetailView: View {
    var video: Favorites
    
    var body: some View {
       
        ScrollView {
        
            VStack {
                AsyncImage(
                    url: URL(string: video.Poster)!,
                   placeholder: { Text("Loading ...") },
                   image: { Image(uiImage: $0).resizable() }
                )
               .frame(idealHeight: UIScreen.main.bounds.width / 2 * 3) // 2:3 aspect ratio
                

                Text(video.Title)
                    .font(.title2)
                    .fontWeight(.bold)
                    .lineLimit(2)
                    .foregroundColor(.secondary)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal)
               
                VStack {
                    Text("Director - \(video.Director)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                    
                    Text("Plot - \(video.Plot)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Year - \(video.Year)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)
                    
                    Text("Rated - \(video.Rated)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Released - \(video.Released)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Runtime - \(video.Runtime)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Genre - \(video.Genre)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Actors - \(video.Actors)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    

                    Text("Language - \(video.Language)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                }.padding()
                
                VStack {
                    
                    Text("Country - \(video.Country)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Awards - \(video.Awards)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)
                    
                    Text("Metascore - \(video.Metascore)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("imdbRating - \(video.imdbRating)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("imdbVotes - \(video.imdbVotes)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("DVD - \(video.DVD)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("BoxOffice - \(video.BoxOffice)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Production - \(video.Production)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)

                    Text("Website - \(video.Website)")
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .padding(20.0)
                    Spacer()
                    
                }.padding()
            }
        }
    }
}

//struct FavoriteDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        FavoriteDetailView()
//    }
//}
