//
//  FetchResult.swift
//  Batman
//
//  Created by Oluwaseun Adebanwo on 11/02/2022.
//

import SwiftUI

class FetchVideo: ObservableObject {
    @Published var videos: [Video] = []
    var imDBIDs: [ImDBID] = []
    
    let apiKey = "18ba084d"
    let pageNumber = 1
    
    func fetch() {
        // construct URL from string
        let urlString = "https://www.omdbapi.com/?apikey=\(apiKey)&s=Batman&page=\(pageNumber)"
        
        
        // Safely convert URL
        if let url = URL(string: urlString) {
            if let data = try? Data(contentsOf: url) {
                // Parse data
                parse(json: data)
            }
        }
        
        // Parse Json Data
        func parse(json: Data) {
            let decoder = JSONDecoder()
            if let jsonSearchResult = try? decoder.decode(SearchResult.self, from: json) {
                imDBIDs = jsonSearchResult.Search
                parseImdbId()
            }
        }
        
        func parseImdbId() {
            for imdbid in imDBIDs {
                let urlImdbIdString = "https://omdbapi.com/?apikey=\(apiKey)&i=\(imdbid.imdbID)"
                
                // Safely convert URL
                if let urlImdb = URL(string: urlImdbIdString) {
                    if let data = try? Data(contentsOf: urlImdb) {
                        parseAndAppend(json: data)
                    }
                }
            }
//            print(videos)
        }
        
        // Parse Json Data and Append
        func parseAndAppend(json: Data) {
            let decoder = JSONDecoder()
            if let jsonSearchResult = try? decoder.decode(Video.self, from: json) {
                print("parseAndAppend")
                videos.append(jsonSearchResult)
            }
        }
    }
}

// Create
// var iMDbs: [ImDBID] = []


// Create Video Array
//                iMDbs = jsonSearchResult.Search

//                var numberOfIds: Int = iMDbs.count
//                let numberOfIds = 10


////loop through Imdb array
//for i in 1...10 {
//    let urlImdbIdString = "https://omdbapi.com/?apikey=\(apiKey)&i=\(iMDbs[i].imdbID)"
//
//    // Safely convert URL
//    if let urlImdb = URL(string: urlImdbIdString) {
//        if let data = try? Data(contentsOf: urlImdb) {
//            let decoder = JSONDecoder()
//            if let jsonIMDBResult = try? decoder.decode(Video.self, from: data) {
//                //Append Video to Video Array
//                videos.append(jsonIMDBResult)
//            }
//
//        }
//    }
//}

//        guard let urlString = URL(string: "https://www.omdbapi.com/?apikey=18ba084d&s=Batman&page=1") else {
//            return
//        }
        
        
        
//        guard let urlImdbIdSearch = URL(string: "https://omdbapi.com/?apikey=18ba084d&i=tt4853102") else {
//            return
//        }
        
        
       
        
//        let task = URLSession.shared.dataTask(with: url) {
//            [weak self] data, _, error in guard let data = data, error == nil else {
//                return
//            }
//
        
//        if let url = URL(string: url){
//            if let data = try? Data(contentsOf: url) {
//
//            }
//        }
        
        // convert to JSON
//        var videoResult: [Video]
//        if let jsonSearchResult = try? JSONDecoder().decode(SearchResult.self, from: data) {
//            videoResult = jsonSearchResult.Search
//            DispatchQueue.main.async {
//                self?.videos = videoResult
//            }
//        }
        
//        do {
////            let videos = try? JSONDecoder().decode(Search.self, from: data)
//
//
//
//        }
//        catch {
//            print(error)
//        }
//        task.resume()
        
//        print("videos: \(self?.videos)")



